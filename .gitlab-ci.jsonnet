local stages = {
  stages: [
    'check',
    'dryrun-check',
    'dryrun',
    'non-prod-cny:deploy',
    'non-prod:deploy',
    'non-prod:QA',
    'gprd-cny:deploy',
    'gprd:deploy:alpha',
    'gprd:deploy:beta',
    'cleanup',
    'scheduled',
  ],
};

local includes = {
  include: [
    // Dependency scanning
    // https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
    { template: 'Jobs/Dependency-Scanning.gitlab-ci.yml' },
    // IaC scanning
    // https://docs.gitlab.com/ee/user/application_security/iac_scanning/
    { template: 'Jobs/SAST-IaC.gitlab-ci.yml' },
    // Secret scanning
    // https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/
    { template: 'Jobs/Secret-Detection.gitlab-ci.yml' },
    { 'local': '.gitlab-ci.includes.yml' },
  ],
};

local exceptCom = {
  'if': '$CI_API_V4_URL == "https://gitlab.com/api/v4"',
  when: 'never',
};

local exceptOps = {
  'if': '$CI_API_V4_URL == "https://ops.gitlab.net/api/v4"',
  when: 'never',
};

local idTokens = {
  id_tokens: {
    VAULT_ID_TOKEN: {
      aud: 'https://vault.gitlab.net',
    },
  },
};

local vaultRw = {
  variables+: {
    VAULT_KUBERNETES_ROLE: 'cluster-admin',
  },
};

local variables = {
  variables: {
    CI_REGISTRY_OPS: 'registry.ops.gitlab.net',
    AUTO_DEPLOY: 'false',
    VAULT_KUBERNETES_ROLE: 'cluster-view',
  },
};

local workflow_rules = {
  workflow: {
    name: '$GITLAB_COM_PIPELINE_NAME',
    rules: [
      {
        // Give a unique name for all Auto-Deployments
        'if': '$AUTO_DEPLOY == "true" && $GITLAB_IMAGE_TAG',
        variables: {
          // These values come from Deployer
          GITLAB_COM_PIPELINE_NAME: 'auto-deploy: $ENVIRONMENT - $GITLAB_IMAGE_TAG',
        },
      },
      {
        // Give a unique name for downstream QA jobs
        'if': '$AUTO_DEPLOY == "false"',
        variables: {
          QA_PIPELINE_NAME: '$CI_PROJECT_PATH: $ENVIRONMENT',
        },
      },
      // Make sure we do branch pipelines only (no duplicate pipelines)
      // https://docs.gitlab.com/ee/ci/yaml/workflow.html#workflowrules-templates
      //{ template: 'Workflows/Branch-Pipelines.gitlab-ci.yml' },
      {
        'if': '$CI_COMMIT_TAG',
      },
      {
        'if': '$CI_COMMIT_BRANCH',
      },
    ],
  },
};

local mainRegion = 'us-east1';

local clusterAttrs = {
  pre: {
    ENVIRONMENT_WITHOUT_STAGE: 'pre',
    GOOGLE_PROJECT: 'gitlab-pre',
    GKE_CLUSTER: 'pre-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
  gstg: {
    ENVIRONMENT_WITHOUT_STAGE: 'gstg',
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
  'gstg-gitlab-36dv2': {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gitlab-36dv2',
    GOOGLE_REGION: mainRegion,
    VAULT_AUTH_CLUSTER: 'gstg-gitlab-36dv2',
  },
  'gstg-us-east1-b': {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-b',
    GOOGLE_ZONE: 'us-east1-b',
  },
  'gstg-us-east1-c': {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-c',
    GOOGLE_ZONE: 'us-east1-c',
  },
  'gstg-us-east1-d': {
    GOOGLE_PROJECT: 'gitlab-staging-1',
    GKE_CLUSTER: 'gstg-us-east1-d',
    GOOGLE_ZONE: 'us-east1-d',
  },
  gprd: {
    ENVIRONMENT_WITHOUT_STAGE: 'gprd',
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-gitlab-gke',
    GOOGLE_REGION: mainRegion,
  },
  'gprd-us-east1-b': {
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-us-east1-b',
    GOOGLE_ZONE: 'us-east1-b',
  },
  'gprd-us-east1-c': {
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-us-east1-c',
    GOOGLE_ZONE: 'us-east1-c',
  },
  'gprd-us-east1-d': {
    GOOGLE_PROJECT: 'gitlab-production',
    GKE_CLUSTER: 'gprd-us-east1-d',
    GOOGLE_ZONE: 'us-east1-d',
  },
};

local baseCiConfigs = {
  '.pre-base': {
    variables: {
      PROJECT: clusterAttrs.pre.GOOGLE_PROJECT,
      REGION: clusterAttrs.pre.GOOGLE_REGION,
      ENVIRONMENT_WITHOUT_STAGE: clusterAttrs.pre.ENVIRONMENT_WITHOUT_STAGE,
    },
    environment: {
      name: 'pre',
      url: 'https://pre.gitlab.com',
    },
  },
  '.pre': {
    extends: [
      '.pre-base',
    ],
    variables: {
      CLUSTER: clusterAttrs.pre.GKE_CLUSTER,
    },
    environment: {
      name: 'pre',
    },
    resource_group: 'pre',
  },
  '.gstg-base': {
    variables: {
      PROJECT: clusterAttrs.gstg.GOOGLE_PROJECT,
      ENVIRONMENT_WITHOUT_STAGE: clusterAttrs.gstg.ENVIRONMENT_WITHOUT_STAGE,
    },
    environment: {
      url: 'https://staging.gitlab.com',
    },
  },
  '.gstg': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs.gstg.GKE_CLUSTER,
      REGION: clusterAttrs.gstg.GOOGLE_REGION,
    },
    environment: {
      name: 'gstg',
    },
    resource_group: 'gstg',
  },
  '.gstg-gitlab-36dv2': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gstg-gitlab-36dv2'].GKE_CLUSTER,
      REGION: clusterAttrs['gstg-gitlab-36dv2'].GOOGLE_REGION,
      VAULT_AUTH_CLUSTER: clusterAttrs['gstg-gitlab-36dv2'].VAULT_AUTH_CLUSTER,
    },
    environment: {
      name: 'gstg-gitlab-36dv2',
    },
    resource_group: 'gstg-gitlab-36dv2',
  },
  '.gstg-us-east1-b': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gstg-us-east1-b'].GKE_CLUSTER,
      REGION: clusterAttrs['gstg-us-east1-b'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gstg-us-east1-b',
    },
    resource_group: 'gstg-us-east1-b',
  },
  '.gstg-us-east1-c': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gstg-us-east1-c'].GKE_CLUSTER,
      REGION: clusterAttrs['gstg-us-east1-c'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gstg-us-east1-c',
    },
    resource_group: 'gstg-us-east1-c',
  },
  '.gstg-us-east1-d': {
    extends: [
      '.gstg-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gstg-us-east1-d'].GKE_CLUSTER,
      REGION: clusterAttrs['gstg-us-east1-d'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gstg-us-east1-d',
    },
    resource_group: 'gstg-us-east1-d',
  },
  '.gprd-base': {
    variables: {
      PROJECT: clusterAttrs.gprd.GOOGLE_PROJECT,
      ENVIRONMENT_WITHOUT_STAGE: clusterAttrs.gprd.ENVIRONMENT_WITHOUT_STAGE,
    },
    environment: {
      url: 'https://gitlab.com',
    },
  },
  '.gprd': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs.gprd.GKE_CLUSTER,
      REGION: clusterAttrs.gprd.GOOGLE_REGION,
    },
    environment: {
      name: 'gprd',
    },
    resource_group: 'gprd',
  },
  '.gprd-us-east1-b': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gprd-us-east1-b'].GKE_CLUSTER,
      REGION: clusterAttrs['gprd-us-east1-b'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gprd-us-east1-b',
    },
    resource_group: 'gprd-us-east1-b',
  },
  '.gprd-us-east1-c': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gprd-us-east1-c'].GKE_CLUSTER,
      REGION: clusterAttrs['gprd-us-east1-c'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gprd-us-east1-c',
    },
    resource_group: 'gprd-us-east1-c',
  },
  '.gprd-us-east1-d': {
    extends: [
      '.gprd-base',
    ],
    variables: {
      CLUSTER: clusterAttrs['gprd-us-east1-d'].GKE_CLUSTER,
      REGION: clusterAttrs['gprd-us-east1-d'].GOOGLE_ZONE,
    },
    environment: {
      name: 'gprd-us-east1-d',
    },
    resource_group: 'gprd-us-east1-d',
  },
};

local onlyAutoDeployFalseAndConfigChanges(environment, cluster) = {
  rules+: [
    {
      'if': '$AUTO_DEPLOY == "true"',
      when: 'never',
    },
    {
      'if': '$CI_PIPELINE_SOURCE == "schedule"',
      when: 'never',
    },
    {
      when: 'on_success',
      changes: [
        'vendor/charts/gitlab/%s/**/*' % environment,
        'vendor/charts/vault-secrets/**/*',
        '.gitlab-ci.yml',
        'bases/helmDefaults.yaml',
        'bases/__base.yaml',
        'bases/%s.yaml' % environment,
        'releases/gitlab/helmfile.yaml',
        'releases/**/values/%s.*' % environment,
        'releases/**/values/%s.*' % cluster,
        'releases/**/values/values*',
        'releases/**/values-sidekiq/values*',
        'releases/**/values-sidekiq/shards/*yaml.gotmpl',
        'releases/**/values-sidekiq/shards/*/%s.*' % environment,
      ],
    },
  ],
};

local onlyGprdMainConfigChanges(isGprdMain) = {
  [if isGprdMain then 'rules']+: [
    {
      'if': '$ENVIRONMENT == "gprd" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule" && $MANUAL_GPRD_DEPLOY == "true"',
      when: 'manual',
      allow_failure: true,
    },
  ],
};

local autoDeployRule(environment) = {
  rules+: [
    {
      'if': '$ENVIRONMENT == "%s" && $DRY_RUN == "false" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"' % environment,
      when: 'on_success',
    },
  ],
};

local checkVendoredCharts = import 'ci/check-vendored-charts.libsonnet';
local shellcheck = import 'ci/shellcheck.libsonnet';
local versionChecks = import 'ci/version-checks.libsonnet';

local assertFormatting = {
  assert_formatting: {
    stage: 'check',
    extends: '.k8s-workloads-image',
    script: |||
      find . -name '*.*sonnet' | xargs -n1 jsonnetfmt -i
      git diff --exit-code
    |||,
    rules: [
      exceptOps,
      { when: 'on_success' },
    ],
  },
};

local ciConfigGenerated = {
  ci_config_generated: {
    stage: 'check',
    extends: '.k8s-workloads-image',
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
    ],
    script: |||
      make generate-ci-config
      git diff --exit-code || (echo "Please run 'make generate-ci-config'" >&2 && exit 1)
    |||,
  },
};

local changeLock = {
  'change-lock': {
    image: '${CI_REGISTRY_OPS}/gitlab-com/gl-infra/change-lock:latest',
    stage: 'check',
    rules: [
      {
        'if': '$CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME',
        when: 'never',
      },
      exceptCom,
    ],
    script: |||
      # Check to ensure that we are not in
      # a change lock period. Set CHANGE_LOCK_OVERRIDE to override check failure.
      change-lock --tags infra
    |||,
  } + onlyAutoDeployFalseAndConfigChanges('gprd', 'gprd*'),
};

local notifyOnFailure = {
  'notify-mr-on-failure': {
    stage: 'cleanup',
    extends: '.k8s-workloads-image',
    rules: [
      {
        'if': '$CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME',
        when: 'never',
      },
      {
        'if': '$EXPEDITE_DEPLOYMENT',
        when: 'never',
      },
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      exceptCom,
      {
        when: 'on_failure',
      },
    ],
    secrets: {
      GITLAB_API_TOKEN: {
        file: false,
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/notify-mr/token@ci',
      },
    },
    allow_failure: true,
    script: |||
      /k8s-workloads/notify-mr -f
    |||,
  } + idTokens,
};

local notifyComMR = {
  notify_com_mr: {
    stage: 'check',
    extends: '.k8s-workloads-image',
    secrets: {
      GITLAB_API_TOKEN: {
        file: false,
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/notify-mr/token@ci',
      },
    },
    script: |||
      /k8s-workloads/notify-mr -s
    |||,
    allow_failure: true,
    rules: [
      {
        'if': '$EXPEDITE_DEPLOYMENT',
        when: 'never',
      },
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      exceptCom,
      {
        when: 'on_success',
      },
    ],
  } + idTokens,
};

local dependencyScanning = {
  dependency_scanning: {
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
    ],
    stage: 'check',
  },
  'gemnasium-dependency_scanning': {
    stage: 'check',
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
    ],
  },
};

local iacScanning = {
  'kics-iac-sast': {
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
    ],
    stage: 'check',
  },
  'iac-sast': {
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
    ],
    stage: 'check',
  },
};

local secretDetection = {
  secret_detection: {
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
    ],
    stage: 'check',
  },
};

local clusterInitBeforeScript = {
  before_script: [
    |||
      if [[ "${LOG_LEVEL}" == "debug" ]]; then
        echo "A high debug level may expose secrets, this job will now exit..."
        exit 1
      fi
    |||,
    'set -e -o pipefail',
    'kube_context="gke_${PROJECT}_${REGION}_${CLUSTER}"',
    'echo "Logging into Vault..."',
    'VAULT_TOKEN="$(vault write -field=token "auth/${VAULT_AUTH_PATH}/login" role="${VAULT_AUTH_ROLE}" jwt="${VAULT_ID_TOKEN}")"; export VAULT_TOKEN',
    'HELM_KUBETOKEN="$(vault write -field=service_account_token "kubernetes/${VAULT_AUTH_CLUSTER:-$CLUSTER}/creds/${VAULT_KUBERNETES_ROLE}" kubernetes_namespace=vault-k8s-secrets cluster_role_binding=true ttl=130m)"; export HELM_KUBETOKEN',
    'kubectl config set-credentials "${VAULT_KUBERNETES_ROLE}" --token="${HELM_KUBETOKEN}"',
    'gke_endpoint="$(vault kv get -mount shared -field endpoint "kubernetes/clusters/${ENVIRONMENT_WITHOUT_STAGE}/${CLUSTER}")"',
    'gke_ca_cert="$(vault kv get -mount shared -field ca_cert "kubernetes/clusters/${ENVIRONMENT_WITHOUT_STAGE}/${CLUSTER}")"',
    'kubectl config set-cluster "${kube_context}" --server="${gke_endpoint}"',
    'kubectl config set "clusters.${kube_context}.certificate-authority-data" "$(echo "${gke_ca_cert}" | base64)"',
    'kubectl config set-context "${kube_context}" --cluster "${kube_context}" --user "${VAULT_KUBERNETES_ROLE}"',
    'kubectl config use-context "${kube_context}"',
  ],
};

local deploy(environment, stage, cluster, ciStage) = {
  local isCanary = stage == 'cny',
  local isGprdMain = environment == 'gprd' && stage == 'main',
  local envPrefix = std.strReplace(environment, '-cny', ''),
  ['%s:dryrun:auto-deploy' % cluster]: {
    stage: 'dryrun',
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
      '.k8s-workloads-image',
    ],
    script: |||
      bin/k-ctl -D %s upgrade
    ||| % if isCanary then '-s cny' else '',
    rules: [
      exceptCom,
      {
        'if': '$ENVIRONMENT == "%s" && $AUTO_DEPLOY == "true" && $CI_PIPELINE_SOURCE != "schedule"' % environment,
        when: 'on_success',
      },
    ],
    tags: [
      'k8s-workloads',
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens,
  ['%s:auto-deploy' % cluster]: {
    stage: ciStage,
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
      '.k8s-workloads-image',
    ],
    script: |||
      bin/k-ctl %s upgrade
    ||| % if isCanary then '-s cny' else '',
    rules: [
      exceptCom,
    ],
    tags: [
      'k8s-workloads',
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens + vaultRw + onlyGprdMainConfigChanges(isGprdMain) + autoDeployRule(environment),
  ['%s:dryrun' % cluster]: {
    stage: 'dryrun',
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
      '.k8s-workloads-image',
    ],
    secrets: {
      GITLAB_API_TOKEN: {
        file: false,
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/notify-mr/token@ci',
      },
    },
    script: |||
      bin/k-ctl -D %s upgrade
    ||| % if isCanary then '-s cny' else '',
    tags: [
      'k8s-workloads',
    ],
    rules: [
      exceptCom,
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens + onlyAutoDeployFalseAndConfigChanges(envPrefix, cluster),
  ['%s:upgrade' % cluster]: {
    stage: ciStage,
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
      '.k8s-workloads-image',
    ],
    variables: {
      DRY_RUN: 'false',
    },
    script: |||
      bin/grafana-annotate -e $CI_ENVIRONMENT_NAME
      bin/k-ctl %s upgrade
    ||| % if isCanary then '-s cny' else '',
    rules: [
      {
        'if': '$CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME',
        when: 'never',
      },
      exceptCom,
    ],
    tags: [
      'k8s-workloads',
    ],
    [if isCanary then 'resource_group']: environment,
  } + clusterInitBeforeScript + idTokens + vaultRw + onlyAutoDeployFalseAndConfigChanges(envPrefix, cluster),
  ['%s:check-label-taxonomy' % cluster]: {
    stage: 'check',
    extends: [
      '.%s' % (if isCanary then envPrefix else cluster),
      '.k8s-workloads-image',
    ],
    script: |||
      bin/k-ctl %s template
      ./bin/check-label-taxonomy.sh
    ||| % if isCanary then '-s cny' else '',
    variables: {
      GITLAB_IMAGE_TAG: '15.test',
      SKIP_REGISTRY_MIGRATION: 'true',
    },
    rules: [
      exceptOps,
    ],
  } + onlyAutoDeployFalseAndConfigChanges(envPrefix, cluster),
};

local qaJob(name, project, allow_failure=false) = {
  ['%s:qa' % name]: {
    stage: 'non-prod:QA',
    [if allow_failure then 'allow_failure']: allow_failure,
    trigger: {
      project: project,
      strategy: 'depend',
      forward: {
        yaml_variables: true,
      },
    },
    variables: {
      SMOKE_ONLY: 'true',
      QA_PIPELINE_NAME: '$QA_PIPELINE_NAME',
    },
    rules: [
      exceptCom,
      {
        'if': '$CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME',
        when: 'never',
      },
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$EXPEDITE_DEPLOYMENT',
        when: 'never',
      },
      {
        'if': '$SKIP_QA == "true"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        when: 'on_success',
        changes: [
          'vendor/charts/gitlab/%s/**/*' % name,
          'vendor/charts/gitlab-runner/%s/**/*' % name,
          'vendor/charts/vault-secrets/**/*',
          '.gitlab-ci.yml',
          '*.yaml',
          '*.yml',
          'bases/helmDefaults.yaml',
          'bases/__base.yaml',
          'bases/%s.yaml' % name,
          'bin/**/*',
          'releases/gitlab/helmfile.yaml',
          'releases/gitlab/values/values*',
          'releases/gitlab/values/%s*' % name,
          'releases/gitlab/values-sidekiq/values*',
          'releases/gitlab/values-sidekiq/shards/*yaml.gotmpl',
          'releases/gitlab/values-sidekiq/shards/*/%s*' % name,
        ],
      },
    ],
  },
};

local openChartBumpMR(name) = {
  ['open-chart-bump-mr-%s' % name]: {
    stage: 'scheduled',
    extends: '.k8s-workloads-image',
    secrets: {
      GITLAB_API_TOKEN: {
        file: false,
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/chart-bumper/token@ci',
      },
      REGISTRY_OPS_GL_USERNAME: {
        file: false,
        vault: 'ops-gitlab-net/charts/helm-ci-ro/user@shared',
      },
      REGISTRY_OPS_GL_PASSWORD: {
        file: false,
        vault: 'ops-gitlab-net/charts/helm-ci-ro/token@shared',
      },
      SSH_PRIVATE_KEY: {
        file: true,
        vault: '${VAULT_SECRETS_PATH}/shared/ssh/private_key@ci',
      },
      SSH_KNOWN_HOSTS: {
        file: true,
        vault: '${VAULT_SECRETS_PATH}/shared/ssh/known_hosts@ci',
      },
    },
    script: |||
      chmod 0400 "${SSH_PRIVATE_KEY}"
      export GIT_SSH_COMMAND="ssh -i ${SSH_PRIVATE_KEY} -o IdentitiesOnly=yes -o GlobalKnownHostsFile=${SSH_KNOWN_HOSTS}"
      git config --global user.email "ops@ops.gitlab.net"
      git config --global user.name "ops-gitlab-net"
      git remote set-url origin https://chart-bumper:${GITLAB_API_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git
      mkdir -p ~/.docker; touch ~/.docker/config.json; chmod 0600 ~/.docker/config.json
      echo -n "${REGISTRY_OPS_GL_USERNAME}:${REGISTRY_OPS_GL_PASSWORD}" \
        | jq -R '{"auths": {"registry.ops.gitlab.net": {"auth": .|@base64}}}' > ~/.docker/config.json
      echo "${GITLAB_API_TOKEN}" | glab auth login --hostname gitlab.com --stdin
      glab config set git_protocol https
      glab auth status
      ./bin/autobump-gitlab-chart.sh %s
    ||| % name,
    rules: [
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
      },
      {
        when: 'never',
      },
    ],
  } + idTokens,
};

local removeExpediteVariable = {
  'remove-expedite-variable': {
    stage: 'cleanup',
    extends: '.k8s-workloads-image',
    secrets: {
      OPS_API_TOKEN: {
        file: false,
        vault: 'access_tokens/${VAULT_SECRETS_PATH}/remove-expedite-variable/token@ci',
      },
    },
    script: |||
      curl --fail --header "PRIVATE-TOKEN: ${OPS_API_TOKEN}" -X DELETE "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables/EXPEDITE_DEPLOYMENT"
    |||,
    rules: [
      {
        'if': '$CI_API_V4_URL == "https://gitlab.com/api/v4"',
        when: 'never',
      },
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        'if': '($CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH) && $EXPEDITE_DEPLOYMENT',
      },
    ],
  } + idTokens,
};

local gitlabCIConf =
  stages
  + shellcheck
  + versionChecks
  + checkVendoredCharts
  + changeLock
  + variables
  + workflow_rules
  + includes
  + notifyComMR
  + baseCiConfigs
  + dependencyScanning
  + iacScanning
  + secretDetection
  + assertFormatting
  + ciConfigGenerated
  + deploy('pre', 'main', 'pre', 'non-prod:deploy')
  + deploy('gstg-cny', 'cny', 'gstg-cny', 'non-prod-cny:deploy')
  + deploy('gstg', 'main', 'gstg', 'non-prod:deploy')
  + deploy('gstg', 'main', 'gstg-gitlab-36dv2', 'non-prod:deploy')
  + deploy('gstg', 'main', 'gstg-us-east1-b', 'non-prod:deploy')
  + deploy('gstg', 'main', 'gstg-us-east1-c', 'non-prod:deploy')
  + deploy('gstg', 'main', 'gstg-us-east1-d', 'non-prod:deploy')
  + deploy('gprd-cny', 'cny', 'gprd-cny', 'gprd-cny:deploy')
  + deploy('gprd', 'main', 'gprd', 'gprd:deploy:alpha')
  + deploy('gprd', 'main', 'gprd-us-east1-b', 'gprd:deploy:alpha')
  + deploy('gprd', 'main', 'gprd-us-east1-c', 'gprd:deploy:beta')
  + deploy('gprd', 'main', 'gprd-us-east1-d', 'gprd:deploy:beta')
  + qaJob('pre', 'gitlab-org/quality/preprod', allow_failure=true)
  + qaJob('gstg', 'gitlab-org/quality/staging')
  + openChartBumpMR('pre')
  + openChartBumpMR('gstg')
  + openChartBumpMR('gprd')
  + openChartBumpMR('bitnami')
  + notifyOnFailure
  + removeExpediteVariable;

std.manifestYamlDoc(gitlabCIConf)
