{
  shellcheck: {
    stage: 'check',
    extends: '.shellcheck-image',
    script: [
      'find bin/ -maxdepth 1 -type f | xargs -r shellcheck -e SC1090,SC1091',
    ],
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_COMMIT_REF_NAME == "master"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        when: 'on_success',
      },
    ],
  },
  shfmt: {
    stage: 'check',
    extends: '.shfmt-image',
    script: [
      'find bin/ -maxdepth 1 -type f | xargs -r shfmt -i 2 -ci -l -d',
    ],
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_COMMIT_REF_NAME == "master"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        when: 'on_success',
      },
    ],
  },
  tooling: {
    stage: 'check',
    extends: '.k8s-workloads-image',
    script: [
      'bin/k-ctl -t',
    ],
    rules: [
      {
        'if': '$AUTO_DEPLOY == "true"',
        when: 'never',
      },
      {
        'if': '$CI_COMMIT_REF_NAME == "master"',
        when: 'never',
      },
      {
        'if': '$CI_PIPELINE_SOURCE == "schedule"',
        when: 'never',
      },
      {
        when: 'on_success',
      },
    ],
  },
}
