## Flagger Canary custom resource

[Flagger](https://github.com/fluxcd/flagger) can be configured to automate the release process for Kubernetes workloads with a custom resource named `Canary`.

### Flagger

Due to its dependency on `Flagger`, the cluster must have `Flagger` installed to support this `Canary` resource. Currently, this is only on `preprod` and `pre-2` clusters. For this reason, it is disabled by default.

`Flagger` is currently defined in [`k8s-mgmt/components/flagger`](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components/-/tree/main/flagger), and modified in [`k8s-mgmt/tenants/delivery/components/flagger`](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/delivery/-/tree/main/components/flagger).

Flagger documentation can be found at [fluxcd.io/flagger](https://fluxcd.io/flagger/), and the `Canary` custom resource schema can be found under [the CRD definition for `flagger`](https://github.com/fluxcd/flagger/blob/main/charts/flagger/crds/crd.yaml).

### Deployment Strategies

There are many deployment strategies that Flagger supports with the use of this `Canary` custom resource.
The current `Canary` template setup supports:

* Canary release (progressive traffic shifting)
* A/B Testing (HTTP headers and cookies traffic routing), and
* Blue/Green Deployments (traffic switching)

They can all be configured by customizing the `analysis` block of the `Canary` resource.

The setup and configuration details can be found at https://docs.flagger.app/usage/deployment-strategies.

### Metrics

As part of the analysis process, Flagger can validate service level objectives (SLOs) like availability, error rate percentage, average response time and any other objective based on app specific metrics. If a drop in performance is noticed during the SLOs analysis, the release will be automatically rolled back with minimum impact to end-users.

The Canary resource comes with two builtin metric checks: HTTP request success rate and duration. These metrics are configurable for services by modifying the values in the [environment's base configuration files](../../bases)

The canary analysis can be extended with custom metric checks (optional). Using a `MetricTemplate` custom resource. Flagger can connect to a metric provider and run a query that returns a float64 value. The query result is used to validate the canary based on the specified threshold range. The `MetricTemplate` is referenced in the `Canary` resource's `metrics` block.

The setup and configuration details can be found at https://docs.flagger.app/usage/metrics. The schema for `MetricTemplate` custom resource can also be found under [the CRD definition under the flagger chart](https://github.com/fluxcd/flagger/blob/main/charts/flagger/crds/crd.yaml).

### Webhooks

Optionally, the canary analysis can be extended with webhooks. Flagger will call each webhook URL and determine from the response status code (HTTP 2xx) if the canary is failing or not.

Types of hooks that can be configured:
* confirm-rollout (manual approval)
* pre-rollout (executed before routing traffic to canary)
* rollout (executed during the analysis on each iteration before the metric checks)
* confirm-traffic-increase (executed before the promotion step)
* post-rollout (executed after the canary has been promoted or rolled back)
* rollback (executed while a canary deployment is in either Progressing or Waiting status)
* event (executed every time Flagger emits a Kubernetes event)

The setup and configuration details can be found at https://docs.flagger.app/usage/webhooks.

#### Load Tests

Optionally, for workloads that are not receiving constant traffic Flagger can be configured with a webhook, that when called, will start a load test for the target workload.

`Flagger` repository provices a load testing service that can be used to generate HTTP and gRPC traffic during canary analysis: https://github.com/fluxcd/flagger/tree/main/charts/loadtester

Follow the usage and setup instructions in https://docs.flagger.app/usage/webhooks#load-testing. After the load test runner is set up, the `webhooks` portion of the `Canary` resource can be customized using values.

#### Integration Tests with Helm Tests

Flagger comes with a testing service that can run Helm tests, Bash Automated Testing System tests, or Concord tests when configured as a webhook. Instructions can be found in https://docs.flagger.app/usage/webhooks#integration-testing.

### Setting up custom values for the `Canary` resource

There are tutorials provided by flagger on how to use and configure the `Canary` resource to:
* [set up Istio Canary Deployment](https://docs.flagger.app/tutorials/istio-progressive-delivery)
* [set up Istio A/B Testing](https://docs.flagger.app/tutorials/istio-progressive-delivery)
* [set up blue/green deployment](https://docs.flagger.app/tutorials/kubernetes-blue-green)

The `Canary` resource is currently set up using the [`raw` chart](./charts/raw/), and is templated under [`gitlab-extras/values.yaml.gotmpl`](./values.yaml.gotmpl). `.Values | getOrNil` is a `helmfile` value, thus they have to go into [`bases/$environment.yaml`](../../bases/) and **not [`releases/gitlab/values`](../../releases/gitlab/values/)**

For example, to set up canary resources for `gitlab` services in the `preprod` cluster, the custom values need to go in [`bases/pre.yaml`](../../bases/pre.yaml):
```
# `bases/pre.yaml`
environments:
  pre:
    values:
      ...
      gitlab_extras:
        flagger_canaries: true
      flagger-canaries:
        gitlab-pages:
          deploymentName: gitlab-gitlab-pages
          progressDeadlineSeconds: 60
          autoscaler: gitlab-gitlab-pages
          service:
            port: 9235
          retries:
            attempts: 3
            perTryTimeout: 1s
            retryOn: "gateway-error,connect-failure,refused-stream"
          analysis:
            interval: 1m
            threshold: 5
            maxWeight: 50
            stepWeight: 10
          customMetrics:
            gitlab-pages-custom-metric:
              templateRef:
                name: gitlab-pages-custom-metric
                namespace: gitlab
              thresholdRange:
                min: 10
                max: 1000
              interval: 1m
          thresholdRanges:
            successRateMin: 99
            latencyMax: 500
          ...
        webservice-api:
          deploymentName: gitlab-webservice-api
          ...
        ...
      ...
```

To see which values are required vs optional for the custom resources mentioned above (`Canary`, `MetricTemplate`, and `AlertProvider`), refer to the schema for flagger crd: https://github.com/fluxcd/flagger/blob/main/charts/flagger/crds/crd.yaml

For example, under `metrics` block of `Canary` crd, although it has many `properties` defined, the only one that is required is the `name` property:

```
...
metrics:
  description: Metric check list for this canary
  type: array
  items:
    type: object
    required: ["name"]
    properties:
      name:
        description: Name of the metric
        type: string
      interval:
        description: Interval of the query
        type: string
        pattern: "^[0-9]+(m|s)"
      threshold:
        description: Max value accepted for this metric
        type: number
...
```
