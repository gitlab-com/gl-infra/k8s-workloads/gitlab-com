---
global:
  redis:
    actioncable:
      host: mymaster
      password:
        enabled: true
        secret: gitlab-redis-pubsub-rails-credential-v1
        key: redis-password
      sentinels:
      - host: redis-pubsub-node-0.redis-pubsub.gitlab-3okls.us-east1.gitlab-production.gke.gitlab.net
        port: 26379
      - host: redis-pubsub-node-1.redis-pubsub.gitlab-3okls.us-east1.gitlab-production.gke.gitlab.net
        port: 26379
      - host: redis-pubsub-node-2.redis-pubsub.gitlab-3okls.us-east1.gitlab-production.gke.gitlab.net
        port: 26379
  hosts:
    # gcloud compute address nginx-gke-gprd-us-east1-b
    externalIP: 10.221.13.25
  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-gprd-us-east1-b
      - 10.221.13.5
    externalHttps:
      # gcloud compute address pages-gke-gprd-us-east1-b
      - 10.221.13.5

gitlab:
  gitlab-shell:
    service:
      # gcloud compute address ssh-gke-gprd-us-east1-b
      loadBalancerIP: 10.221.13.40
  webservice:
    deployments:
      api:
        service:
          # gcloud compute address api-gke-gprd-us-east1-b
          loadBalancerIP: 10.221.13.18
      git:
        service:
          # gcloud compute address git-https-gke-gprd-us-east1-b
          loadBalancerIP: 10.221.13.9
      web:
        service:
          # gcloud compute address web-gke-gprd-us-east1-b
          loadBalancerIP: 10.221.13.65
      websockets:
        service:
          # gcloud compute address websockets-gke-gprd-us-east1-b
          loadBalancerIP: 10.221.13.19
      ai-assisted:
        workhorse:
          keywatcher: false
        hpa:
          minReplicas: 3
          cpu:
            targetType: Utilization
            targetAverageUtilization: 65
        resources:
          limits:
            memory: 8.0G
          requests:
            cpu: 4
            memory: 6G
        service:
          # gcloud compute address ai-assisted-gke-gprd-us-east1-b
          loadBalancerIP: 10.221.13.27
          type: LoadBalancer
          loadBalancerSourceRanges:
            - 10.0.0.0/8
          annotations:
            # !! Warning !! Changing these annotations will likely result in a delete action on the forwarding rule.
            # See https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8042
            cloud.google.com/load-balancer-type: Internal
            networking.gke.io/internal-load-balancer-allow-global-access: "true"
        common:
          labels:
            type: ai-assisted
        deployment:
          terminationGracePeriodSeconds: 65
        extraEnv:
          GITLAB_METRICS_INITIALIZE: "ai-assisted"
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-ai-assisted
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"ai-assisted\", \"stage\": \"main\"}"
          ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
        shutdown:
          blackoutSeconds: 10
        ingress:
          enabled: true
          path: "/api/v4/code_suggestions/completions"
        pod:
          labels:
            deployment: ai-assisted
            type: ai-assisted
        workerProcesses: 4

registry:
  service:
    # gcloud compute address registry-gke-gprd-us-east1-b
    loadBalancerIP: 10.221.13.41

gitlab-zoekt:
  install: false
