---

gitlab-runner:
  gitlabUrl: {{ .Values.gitlab_endpoint }}
  install: true
  rbac:
    create: true
  runners:
    config: |
      [[runners]]
        [runners.kubernetes]
          namespace = "gitlab"
          image = "debian:buster-slim"
          privileged = true
          allow_privilege_escalation = true
        [[runners.kubernetes.volumes.empty_dir]]
          name = "docker-certs"
          mount_path = "/certs/client"
          medium = "Memory"

# Force PreProd config for https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/1163
nginx-ingress:
  controller:
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                  - key: app.kubernetes.io/name
                    operator: In
                    values:
                      - ingress-nginx
                  - key: app.kubernetes.io/instance
                    operator: In
                    values:
                      - ingress-nginx
                  - key: app.kubernetes.io/component
                    operator: In
                    values:
                      - controller
              topologyKey: kubernetes.io/hostname

registry:
  middleware:
    storage:
      - name: googlecdn
        options:
          baseurl: cdn.registry.pre.gitlab-static.net
          privatekeySecret:
            secret: registry-cdn-private-key-v2
            key: private-key
          keyname: pre-registry-cdn
          ipfilteredby: gcp
  database:
    enabled: true
    host: 10.33.1.2
    user: registry
    name: registry_production
    password:
      secret: registry-postgresql-password-v2
    pool:
      maxopen: 5
      maxidle: 5
      maxlifetime: 5m
  extraEnv:
    SKIP_POST_DEPLOYMENT_MIGRATIONS: true
    REGISTRY_FF_ACCURATE_LAYER_MEDIA_TYPES: true
    # https://gitlab.com/gitlab-org/container-registry/-/issues/1036
    REGISTRY_FF_ONGOING_RENAME_CHECK: true
    # https://gitlab.com/gitlab-org/container-registry/-/issues/1438
    REGISTRY_FF_ENFORCE_LOCKFILES: true
  gc:
    disabled: false
    maxbackoff: 30m
    reviewafter: 5m
    noidlebackoff: true
    blobs:
      storagetimeout: 2s
  service:
    # gcloud compute address registry-gke-pre
    loadBalancerIP: 10.232.20.8
  maintenance:
    uploadpurging:
      enabled: false
  validation:
    disabled: false
    manifests:
      referencelimit: 200
      payloadsizelimit: 256000
      urls:
        # this is needed to keep backwards compatibility when `validation.disabled: false`, as by default an empty `allow` means no URLs are allowed
        allow:
          - .*
  redis:
    cache:
      enabled: true
      host: mymaster
      sentinels:
        - host: redis-registry-cache-node-0.redis-registry-cache.pre.gke.gitlab.net
          port: 26379
        - host: redis-registry-cache-node-1.redis-registry-cache.pre.gke.gitlab.net
          port: 26379
        - host: redis-registry-cache-node-2.redis-registry-cache.pre.gke.gitlab.net
          port: 26379
      password:
        enabled: true
        secret: redis-registry-cache-password-v1
        key: redis-password
      dialtimeout: 2s
      readtimeout: 2s
      writetimeout: 2s
      pool:
        size: 10
        maxlifetime: 1h
        idletimeout: 5m
    rateLimiting:
      enabled: true
      cluster:
        - host: redis-cluster-registry-rl-shard-01-01-db-pre.c.gitlab-pre.internal
          port: 6379
        - host: redis-cluster-registry-rl-shard-02-01-db-pre.c.gitlab-pre.internal
          port: 6379
        - host: redis-cluster-registry-rl-shard-03-01-db-pre.c.gitlab-pre.internal
          port: 6379
      password:
        enabled: true
        secret: redis-registry-rl-password-v1
        key: redis-registry-rl-password
      username: registry
      db: 0
      dialtimeout: 2s
      readtimeout: 2s
      writetimeout: 2s
      pool:
        size: 10
        maxlifetime: 1h
        idletimeout: 5m
  log:
    level: debug
  storage:
    secret: registry-storage-v7
    extraKey:

  networkpolicy:
    egress:
      # The following rules enable traffic to all external
      # endpoints, except the metadata service and the local
      # network (except DNS and DB requests)
      rules:
        - to:
            - ipBlock:
                cidr: 10.0.0.0/8
          ports:
            # cloudsql in pre
            - port: 5432
              protocol: TCP
            # pgbouncer in gstg and gprd
            - port: 6432
              protocol: TCP

        # For redis access
        - ports:
            - port: 6379
              protocol: TCP
            - port: 26379
              protocol: TCP

        - ports:
            - port: 53
              protocol: UDP
            - port: 53
              protocol: TCP

        # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
        - to:
            - ipBlock:
                cidr: 169.254.169.252/32
          ports:
            - port: 988
              protocol: TCP
        - to:
            - ipBlock:
                cidr: 169.254.169.254/32
          ports:
            - port: 80
              protocol: TCP

        - to:
            - ipBlock:
                cidr: 0.0.0.0/0
                except:
                  - 10.0.0.0/8
                  - 169.254.169.252/32
                  - 169.254.169.254/32

gitlab:
  gitaly:
    metrics:
      enabled: true
      serviceMonitor:
        enabled: true
        additionalLabels:
          gitlab.com/prometheus-instance: prometheus-system
        endpointConfig:
          relabelings:
            - action: labelmap
              regex: "__meta_kubernetes_pod_label_(.+)"
            - sourceLabels:
                - __meta_kubernetes_pod_node_name
              targetLabel: node
            - targetLabel: job
              replacement: gitaly
            - action: replace
              replacement: gitaly
              targetLabel: type
            - action: replace
              targetLabel: fqdn
              sourceLabels:
                - __meta_kubernetes_pod_name

    annotations:
      cluster-autoscaler.kubernetes.io/safe-to-evict: "false"

    workhorse:
      serviceName: webservice-api

    resources:
      requests:
        cpu: 2000m
        memory: 8Gi
      limits:
        cpu: 2000m
        memory: 8Gi

    init:
      resources:
        requests:
          cpu: 50m
          memory: 32Mi
        limits:
          cpu: 50m
          memory: 32Mi

    priorityClassName: gitlab-gitaly

    antiAffinity: hard
    affinity:
      podAntiAffinity:
        topologyKey: kubernetes.io/hostname

    securityContext:
      fsGroupChangePolicy: OnRootMismatch
      seccompProfile: ""

    gracefulRestartTimeout: 1

    # TODO: review after https://gitlab.com/gitlab-org/charts/gitlab/-/merge_requests/3850
    statefulset:
      livenessProbe:
        initialDelaySeconds: 0
        periodSeconds: 10
      readinessProbe:
        initialDelaySeconds: 0
        periodSeconds: 5
      startupProbe:
        enabled: true

    persistence:
      size: 200Gi
      storageClass: pd-balanced

    cgroups:
      enabled: true
      memoryBytes: 7516192768 # 7 GiB
      cpuShares: 1024
      cpuQuotaUs: 200000 # 2 vCPU
      repositories:
        count: 20
        memoryBytes: 4294967296 # 4 GiB
        cpuShares: 512
        cpuQuotaUs: 150000 # 1.5 vCPU

  gitlab-pages:
    hpa:
      minReplicas: 1
      cpu:
        targetAverageValue: 400m
    resources:
      requests:
        cpu: 500m
        memory: 70M
      limits:
        memory: 1G
    extraEnv:
      FF_HANDLE_CACHE_HEADERS: "true"
      FF_ENFORCE_IP_RATE_LIMITS: "true"
      FF_ENFORCE_DOMAIN_RATE_LIMITS: "true"
      FF_CONFIGURABLE_ROOT_DIR: "true"
      FF_ENABLE_DOMAIN_REDIRECT: "true"
      FF_ENABLE_PROJECT_PREFIX_COOKIE_PATH: "true"
      FF_RATE_LIMITER_CLOSE_CONNECTION: "true"
    rateLimitSourceIP: 20
    rateLimitSourceIPBurst: 300
    rateLimitDomain: 100
    rateLimitDomainBurst: 500
    rateLimitTLSDomain: 30
    rateLimitTLSDomainBurst: 100
    zipCache:
      expiration: 300s
    networkpolicy:
      egress:
        rules:
          - to:
            - podSelector:
                matchLabels:
                  app: webservice
                  type: internal-api
            ports:
              - port: 8181
                protocol: TCP

          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          # Allow traffic to storage.googleapis.com to get zip stores
          - ports:
              - port: 443
                protocol: TCP
  webservice:
    hpa:
      minReplicas: 2
      maxReplicas: 5
    # blackoutSeconds, minReplicas, initialDelaySeconds, terminationGracePeriodSeconds
    # set temporarily for
    # testing https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1509
    shutdown:
      blackoutSeconds: 0
    deployments:
      api:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"api\", \"stage\": \"main\"}"
        ingress:
          path: '/api'
        service:
          # gcloud compute address api-gke-pre
          loadBalancerIP: 10.232.20.11
        resources:
          limits:
            memory: 8.0G
          requests:
            memory: 6.0G
      git:
        service:
          # gcloud compute address git-https-gke-pre
          loadBalancerIP: 10.232.20.5
        shutdown:
          blackoutSeconds: 0
      web:
        service:
          # gcloud compute address web-gke-pre
          loadBalancerIP: 10.232.20.3
        resources:
          limits:
            memory: 8.0G
          requests:
            memory: 6.0G
      websockets:
        deployment:
          terminationGracePeriodSeconds: 30
        service:
          # gcloud compute address websockets-gke-pre
          loadBalancerIP: 10.232.20.6
      internal-api:
        deployment:
          terminationGracePeriodSeconds: 65
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"internal-api\", \"stage\": \"main\"}"
        shutdown:
          blackoutSeconds: 10
        hpa:
          minReplicas: 2
          maxReplicas: 5
        resources:
          requests:
            cpu: 500m
            memory: 1.25G
        workhorse:
          extraArgs: '-apiLimit 9 -apiQueueDuration 30s -apiQueueLimit 2000 -apiCiLongPollingDuration 50s -propagateCorrelationID'
        workerProcesses: 6
    rack_attack:
      git_basic_auth:
        enabled: false
    extraEnv:
      ENABLE_RBTRACE: 1
    workerProcesses: 2
    workhorse:
      resources:
        limits:
          memory: 1G
        requests:
          cpu: 100m
          memory: 50M
    resources:
      limits:
        memory: 4.0G
      requests:
        cpu: 1
        memory: 1.25G
    # TODO: remove after testing, move to `values.yaml.gotmpl`
    monitoring:
      ipWhitelist:
        - "10.0.0.0/8"
        - "127.0.0.0/8"
        - "169.254.169.252/24"
        # Google LoadBalancer Health Check IP Range
        # Ref: https://cloud.google.com/load-balancing/docs/health-checks#fw-rule
        - "35.191.0.0/16"
        - "130.211.0.0/22"
    networkpolicy:
      egress:
        # The following rules enable traffic to all external
        # endpoints, except the metadata service and the local
        # network (except DNS, gitaly, redis and postgres)
        rules:
          # Allow all traffic except internal network and metadata service
          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                  - 10.0.0.0/8
                  - 169.254.169.252/32
                  - 169.254.169.254/32

          # Allow traffic for DNS + consul DNS
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP
              - port: 8600
                protocol: TCP
              - port: 8600
                protocol: UDP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          # Allow internal traffic to Container Registry
          - to:
              - podSelector:
                  matchLabels:
                    app: registry
            ports:
              - port: 5000
                protocol: TCP

          # Allow internal traffic from API nodes to Kas
          - to:
              - podSelector:
                  matchLabels:
                    app: kas
            ports:
              - port: 8153
                protocol: TCP

          # Allow traffic to Redis
          - ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP
              # gstg, gprd Redis
              - port: 26379
                protocol: TCP

          # Allow traffic to Postgresql
          - ports:
              # pre CloudSQL
              - port: 5432
                protocol: TCP
              # gstg, gprd pgbouncer
              - port: 6432
                protocol: TCP
              - port: 6433
                protocol: TCP
              - port: 6434
                protocol: TCP
              - port: 6435
                protocol: TCP
              - port: 6436
                protocol: TCP
              - port: 6437
                protocol: TCP
              - port: 6438
                protocol: TCP
              - port: 6439
                protocol: TCP
              - port: 6440
                protocol: TCP
              - port: 6441
                protocol: TCP
              - port: 6442
                protocol: TCP
              - port: 6443
                protocol: TCP

          # Allow traffic to Gitaly
          - ports:
              # Gitaly k8s
              - port: 8075
                protocol: TCP
              # Gitaly non-TLS
              - port: 9999
                protocol: TCP
              # Gitaly TLS
              - port: 9998
                protocol: TCP
              # Praefect
              - port: 2305
                protocol: TCP

          # Allow internal traffic to thanos
          - ports:
              - port: 9090
                protocol: TCP

          # Allow internal traffic to Mimir
          - to:
            - ipBlock:
                cidr: 10.250.26.38/32
            ports:
            - port: 443
              protocol: TCP

  gitlab-shell:
    workhorse:
      serviceName: webservice-internal-api
    extraEnv:
      GITLAB_CONTINUOUS_PROFILING: stackdriver?service=gitlab-shell
    metrics:
      enabled: true
    minReplicas: 2
    maxReplicas: 5
    service:
      # gcloud compute address ssh-gke-pre
      loadBalancerIP: 10.232.20.9
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-gitlab-shell@{{ .Values.google_project }}.iam.gserviceaccount.com
    sshDaemon: gitlab-sshd

  mailroom:
    workhorse:
      serviceName: webservice-api
    networkpolicy:
      egress:
        rules:
          # Allow DNS
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          # Allow IMAP
          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                    - 10.0.0.0/8
                    - 169.254.169.252/32
                    - 169.254.169.254/32
            ports:
              - port: 993
                protocol: TCP

          # Allow Redis
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP

              # gstg, gprd
              - port: 26379
                protocol: TCP

          # Allow talking to our internal API endpoints
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              - port: 443
                protocol: TCP
              - port: 11443
                protocol: TCP
              - port: 8181
                protocol: TCP

  sidekiq:
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-sidekiq@{{ .Values.google_project }}.iam.gserviceaccount.com
    pods:
      - name: catchall
        common:
          labels:
            shard: catchall
        concurrency: 25
        keda:
          minReplicaCount: 1
          maxReplicaCount: 5
          pollingInterval: 15
          behavior:
            scaleDown:
              stabilizationWindowSeconds: 300
              policies:
                - type: Percent
                  value: 10
                  periodSeconds: 15
            scaleUp:
              policies:
                - type: Percent
                  value: 100
                  periodSeconds: 15
          triggers:
            - type: cpu
              metricType: AverageValue
              metadata:
                value: 450m
            - type: prometheus
              metricType: Value
              metadata:
                serverAddress: https://mimir-internal.ops.gke.gitlab.net/prometheus
                customHeaders: X-Scope-OrgID=gitlab-{{ .Environment.Values.env_prefix }}
                query: sum(avg_over_time(sidekiq_running_jobs{shard="catchall", namespace={{ .Release.Namespace | quote }}, cluster={{ .Environment.Values.cluster | quote }}}[15s])) / sum(avg_over_time(sidekiq_concurrency{shard="catchall", namespace={{ .Release.Namespace | quote }}, cluster={{ .Environment.Values.cluster | quote }}}[15s]))
                threshold: '0.75'
                ignoreNullValues: 'false'
                authModes: basic
              authenticationRef:
                name: keda-mimir-creds
                kind: ClusterTriggerAuthentication
            - type: cron
              metadata:
                timezone: UTC
                start: 55 * * * *
                end: 10 * * * *
                desiredReplicas: "3"
    extraEnv:
      GITLAB_MEMORY_WATCHDOG_ENABLED: "true"
    psql:
      host: 10.33.0.48
    networkpolicy:
      egress:
        # The following rules enable traffic to all external
        # endpoints, except the metadata service and the local
        # network (except DNS, gitaly, redis and postgres)
        rules:
          # Allow all traffic except internal network and metadata service
          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                  - 10.0.0.0/8
                  - 169.254.169.252/32
                  - 169.254.169.254/32

          # Allow DNS traffic to the metadata server
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          # Allow internal traffic to consul for consul DNS
          - to:
              - namespaceSelector: {}
                podSelector:
                  matchLabels:
                    app: consul
            ports:
              - port: 8600
                protocol: TCP
              - port: 8600
                protocol: UDP

          # Allow internal traffic to Redis
          - ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP
              # gstg, gprd Redis
              - port: 26379
                protocol: TCP

          # Allow internal traffic to Postgresql
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # pre CloudSQL
              - port: 5432
                protocol: TCP
              # gstg, gprd pgbouncer
              - port: 6432
                protocol: TCP
              - port: 6433
                protocol: TCP
              - port: 6434
                protocol: TCP
              - port: 6435
                protocol: TCP
              - port: 6436
                protocol: TCP
              - port: 6437
                protocol: TCP
              - port: 6438
                protocol: TCP
              - port: 6439
                protocol: TCP
              - port: 6440
                protocol: TCP
              - port: 6441
                protocol: TCP
              - port: 6442
                protocol: TCP
              - port: 6443
                protocol: TCP

          # Allow internal traffic to Gitaly
          - ports:
              # Gitaly k8s
              - port: 8075
                protocol: TCP
              # Gitaly non-TLS
              - port: 9999
                protocol: TCP
              # Gitaly TLS
              - port: 9998
                protocol: TCP
              # Praefect
              - port: 2305
                protocol: TCP

          # Allow internal traffic to Container Registry
          - ports:
              - port: 5000
                protocol: TCP

            # Allow internal traffic to thanos
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              - port: 9090
                protocol: TCP

          # Allow internal traffic to Mimir
          - to:
            - ipBlock:
                cidr: 10.250.26.38/32
            ports:
            - port: 443
              protocol: TCP

  kas:
    minReplicas: 2
    maxReplicas: 5
    service:
      # gcloud compute address kas-internal-gke-pre
      loadBalancerIP: 10.232.20.4
    workhorse:
      host: 'gitlab-webservice-api.gitlab.svc'
    websocketToken:
      secret: kas-websocket-token-v1
      key: kas_websocket_token_secret
    networkpolicy:
      egress:
        rules:
          # Disables all outgoing traffic except to specific ports on the internal network
          # Allow traffic for DNS
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP
          # Allow talking to our internal API endpoints
          - to:
              - podSelector:
                  matchLabels:
                    gitlab.com/webservice-name: api
            ports:
              - port: 8181
                protocol: TCP
          # Allow internal traffic to Gitaly
          - ports:
              # Gitaly k8s
              - port: 8075
                protocol: TCP
              # Gitaly non-TLS
              - port: 9999
                protocol: TCP
              # Gitaly TLS
              - port: 9998
                protocol: TCP
              # Praefect
              - port: 2305
                protocol: TCP
          # Allow internal traffic to Redis
          - ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP
          # Allow all outgoing HTTPS/443
          - ports:
              - port: 443
                protocol: TCP
          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP
          # KAS internal traffic
          - to:
              - podSelector:
                  matchLabels:
                    app: kas
            ports:
              - port: 8155
                protocol: TCP
global:
  ingress:
    tls:
      secretName: gitlab-pages-tls-certificate-v4
  appConfig:
    artifacts:
      bucket: gitlab-pre-artifacts
      cdn:
        secret: artifacts-cdn-private-key-v5
    contentSecurityPolicy:
      enabled: true
      report_only: false
      directives:
        report_uri: "https://new-sentry.gitlab.net/api/4/security/?sentry_key=f5573e26de8f4293b285e556c35dfd6e&sentry_environment=pre"
    dependencyProxy:
      enabled: true
      bucket: gitlab-pre-dependency-proxy
    externalDiffs:
      bucket: gitlab-pre-external-diffs
    incomingEmail:
      address: "incoming-pre+%{key}@incoming.gitlab.com"
      user: incoming-pre@incoming.gitlab.com
      deliveryMethod: webhook
    lfs:
      bucket: gitlab-pre-lfs-objects
    object_store:
      connection:
        secret: gitlab-object-storage-v3
    omniauth:
      providers:
        - secret: gitlab-google-oauth2-v2
    packages:
      bucket: gitlab-pre-package-repo
    sentry:
      clientside_dsn: https://f5573e26de8f4293b285e556c35dfd6e@new-sentry.gitlab.net/4
      dsn: https://dafccd342c484bf9a1d2c20f769eb9cc@new-sentry.gitlab.net/3
      environment: pre
    terraformState:
      bucket: gitlab-pre-terraform-state
    uploads:
      bucket: gitlab-pre-uploads

  email:
    from: notify@mg.pre.gitlab.com
    reply_to: noreply@pre.gitlab.com

  gitaly:
    enabled: true
    # Kubernetes STS
    internal:
      names:
        # STS instances start from 0
        - gitaly-k8s-00-stor-pre
        - gitaly-k8s-01-stor-pre
    # VMs
    external:
      - hostname: gitaly-01-sv-pre.c.gitlab-pre.internal
        name: default
        port: "9999"
        tlsEnabled: false
      - hostname: praefect-01-stor-pre.c.gitlab-pre.internal
        name: praefect
        port: "2305"
        tlsEnabled: false
      - hostname: gitaly-03-sv-pre.c.gitlab-pre.internal
        name: gitaly-03
        port: "9999"
        tlsEnabled: false

  hosts:
    # gcloud compute address nginx-gke-pre
    externalIP: 10.232.20.7
    gitlab:
      name: pre.gitlab.com
    kas:
      name: kas.pre.gitlab.com
    registry:
      name: registry.pre.gitlab.com

  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-pre
      - 10.232.20.10
    externalHttps:
      # gcloud compute address pages-gke-pre
      - 10.232.20.10
    host: pre.gitlab.io
    objectStore:
      connection:
        secret: gitlab-object-storage-v3

  psql:
    host: 10.33.0.48

  redis:
    # GCP MemoryStore
    host: 10.232.7.3
    port: "6379"
    rateLimiting:
      user: rails
      password:
        enabled: true
        secret: gitlab-redis-cluster-ratelimiting-rails-credential-v2
        key: password
      cluster:
      - host: redis-cluster-ratelimiting-shard-01-01-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-01-02-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-01-03-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-02-01-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-02-02-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-02-03-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-03-01-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-03-02-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-03-03-db-pre.c.gitlab-pre.internal
    repositoryCache:
      host: pre-redis-repository-cache
      password:
        enabled: true
        secret: gitlab-redis-credential-v2
        key: password
      sentinels:
      - host: redis-repository-cache-01-db-pre.c.gitlab-pre.internal
        port: 26379
      - host: redis-repository-cache-02-db-pre.c.gitlab-pre.internal
        port: 26379
      - host: redis-repository-cache-03-db-pre.c.gitlab-pre.internal
        port: 26379

  registry:
    notifications:
      endpoints:
        - name: gitlab
          backoff: 1s
          headers:
            Authorization:
              secret: gitlab-registry-notification-v2
              key: secret
          ignore:
            actions:
              - pull
          ignoredmediatypes:
            - application/octet-stream
          maxretries: 5
          timeout: 500ms
          url: {{ .Values | getOrNil "gitlab_endpoint" }}/api/v4/container_registry_event/events

  runner:
    registrationToken:
      secret: gitlab-runner-registration-credential-v2

  smtp:
    domain: mg.pre.gitlab.com
    user_name: postmaster@mg.pre.gitlab.com

gitlab-zoekt:
  networkpolicy:
    egress:
      rules:
        - to:
            - ipBlock: # Allow all egress to the internet but nothing internal. Needed to clone repos from gitlab.com. We could be more specific if we have an IP range for gitlab.com public interface for cloning
                cidr: 0.0.0.0/0
                except:
                - 10.0.0.0/8
                - 192.168.0.0/16
                - 172.16.0.0/12
                - 169.254.169.252/32
                - 169.254.169.254/32
          ports:
            - protocol: TCP
              port: 8443
        # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
        - to:
            - ipBlock:
                cidr: 169.254.169.252/32
          ports:
            - port: 988
              protocol: TCP
        - to:
            - ipBlock:
                cidr: 169.254.169.254/32
          ports:
            - port: 80
              protocol: TCP
        # Allow DNS
        - ports:
            - port: 53
              protocol: UDP
            - port: 53
              protocol: TCP
