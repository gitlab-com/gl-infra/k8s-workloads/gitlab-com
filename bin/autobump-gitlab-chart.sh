#!/bin/bash

set -euo pipefail

DELIVERY_SRE_ACCOUNTS=("skarbek" "dat.tang.gitlab" "jennykim-gitlab" "anganga")

bump_gitlab_chart() {
  # Get current chart ver for environment
  ENV_CHART_VER=$(yq e ".directories[0].contents[] | select(.path == \"gitlab/${ENVIRONMENT}\").git.ref" vendir.yml)

  # Get chart version from dev.gitlab.org
  UPSTREAM_VER=$(git ls-remote git@dev.gitlab.org:gitlab/charts/gitlab.git HEAD | awk '{ print $1}')

  if [[ ${ENV_CHART_VER} != "${UPSTREAM_VER}" ]]; then
    yq -i e ".directories[0].contents |= map(select(.path == \"gitlab/${ENVIRONMENT}\").git.ref = \"${UPSTREAM_VER}\")" vendir.yml
    vendir sync
    pushd "vendor/charts/gitlab/${ENVIRONMENT}"
    helm dep update
    popd
    git checkout -b "${ENVIRONMENT}-chart-bump-${UPSTREAM_VER}"
    git add vendir.yml vendir.lock.yml
    git add "vendor/charts/gitlab/${ENVIRONMENT}"
    git commit -m "Bump to Gitlab chart ${UPSTREAM_VER} in ${ENVIRONMENT}

Changes can be viewed at

https://gitlab.com/gitlab-org/charts/gitlab/-/compare/${ENV_CHART_VER}...${UPSTREAM_VER}"
    command='glab mr create -f -y --squash-before-merge --remove-source-branch --push -l "automation:bot-authored"'
    for value in "${DELIVERY_SRE_ACCOUNTS[@]}"; do
      command+=" -a $value"
    done
    eval "$command"
  else
    echo "ENV_CHART_VER and UPSTREAM_VER are the same. Nothing to bump."
  fi
}

bump_bitnami_chart() {
  ENV_CHART_VER=$(yq e ".appVersion" vendor/charts/vault-secrets/charts/common/Chart.yaml)
  vendir sync -d vendor/charts/vault-secrets
  UPSTREAM_VER=$(yq e ".appVersion" vendor/charts/vault-secrets/charts/common/Chart.yaml)

  if [[ ${ENV_CHART_VER} != "${UPSTREAM_VER}" ]]; then
    git checkout -b "bitnami-chart-bump-${UPSTREAM_VER}"
    git add vendor/charts/vault-secrets/Chart.lock vendor/charts/vault-secrets/charts/common/
    git commit -m "Bump to Bitnami chart ${UPSTREAM_VER} in vault-secrets

Changes can be viewed at

https://github.com/bitnami/charts/tree/common/${UPSTREAM_VER}/bitnami/common}"
    command='glab mr create -f -y --squash-before-merge --remove-source-branch --push -l "automation:bot-authored"'
    for value in "${DELIVERY_SRE_ACCOUNTS[@]}"; do
      command+=" -a $value"
    done
    eval "$command"
  else
    echo "ENV_CHART_VER and UPSTREAM_VER are the same. Nothing to bump."
  fi
}

usage() {
  cat <<EOF
$0: Automatically looks for the latest version of the Gitlab and bitnami/common charts in git and
opens a merge request for the specified environment to bump the chart to that version.

USAGE:
$0 environment

e.g. $0 gstg | gprd | pre | bitnami
EOF
}

main() {
  if [[ $# -ne 1 ]]; then
    usage
    exit 1
  else
    ENVIRONMENT=${1}
    if [[ "$ENVIRONMENT" == "bitnami" ]]; then
      bump_bitnami_chart
    else
      bump_gitlab_chart
    fi
  fi
}

main "$@"
